# Zadanie rekrutacyjne Morizon

Chcielibyśmy abyś, jako formę prezentacji Twoich umiejętności, zrefaktoryzował oraz rozbudował już istniejącą aplikację.
Jest ona już wstępnie skonfigurowana i można ją uruchomić za pomocą docker-a lub też w dowolny inny sposób, który preferujesz (docker ma Ci ułatwić pracę, ale jeżeli tak nie będzie, nie musisz go używać).

### Uruchomienie apliakcji

 - **Docker** - w głównym katalogu projektu uruchamiamy polecenie `docker-compose up --build`, w ramach stack-a zostanie uruchomiony również serwer MySQL, do którego aplikacja będzie podłączona 
 - **PHP** - aplikacje możesz też uruchomić na wbudowanym serwerze php: `DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7" php -S 0.0.0.0:8081 -t public/`, jednak pamiętaj że musisz dodatkowo uruchomić sobie bazę danych, a wszystkie potrzebne dane do logowania przekazać w zmiennej środowiskowej `DATABASE_URL`

W obu przypadkach aplikacja będzie dostępna pod adresem `http://127.0.0.1:8081/`

### Uruchomienie bazy danych

Inicjalizację tabeli i wypełnienie przykładowymi danymi można wykonać za pomocą DoctrineMigrations przez polecenie:
`./bin/console doctrine:migrations:migrate`

W aplikacji uruchomionej przez `docker-compose` należy wykonać to polecenie z wnętrza kontenera aplikacji.

### Użycie

Obecnie aplikacja posiada jeden endpoint, który przyjmuje z GET-a 3 parametry: `id`, `name` oraz `price` i na ich podstawie tworzy nowy rekord w tabeli `product`.

## Zadania dla Ciebie
Po pierwsze, (jak pewnie zauważysz) kod jest brzydki i nie do końca działa zgodnie z oczekiwaniami, więc wymaga refaktoryzacji.

Po drugie, chcemy abyś dodał możliwość:
 - podania cen w 2 różnych walutach (dowolnych)
 - dodania długiego opisu (do 10000 znaków) 
 - kasowania produktów po id 
 - wyświetlania produktów po id
 - listowania produktów (tutaj wystarczy jak dodasz limit, nie musisz robić stronicowania)

## Chętnie zobaczylibyśmy jak w praktyce stosujesz/używasz

- PHP 7.4 / 8 (wszystko nowe co się tam pojawiło, mile widziane)
- DDD
- SOLID
- wzorce projektowe:
  - fabryki
  - buildery
  - fasady 
  - repozytoria
  - inne też mile widziane
- Interface
- DI container
- Doctrine
- Doctrine Migrations
- PHPUnit
  - Unit testy
  - testy integracyjne
- CQRS
  - podział na komendy i query
  - asynchroniczne przetwarzanie komend
  - osobny model dla query
- REST/GraphQL
- framework agnostic
- Docker

## Czego oczekujemy?

Tego, że pokażesz nam jak najwięcej swoich umiejętności programistycznych, znajomości wzorców oraz różnych bibliotek i narzędzi.
Jeżeli czegoś nie znasz, ale w trakcie robienia zadania o tym doczytasz i poznasz, to śmiało możesz tego użyć, ale wiedz, że w późniejszym etapie możemy o to zapytać.

## Ile czasu powinno Ci to zająć?

Tutaj wszystko zależy od tego jak dużo umiesz i jak płynnie się w tym poruszasz. Jeżeli zrobisz to w dzień, super, jeżeli będziesz potrzebował kilku dni, to też nie problem.

**Nie musisz zrobić wszystkiego** z tej listy, ani użyć wszystkich rzeczy które wymieniliśmy (nie musisz się też do niej ograniczać). 
Jeżeli dasz radę zaprezentować wszystkie swoje umiejętności robiąc np. tylko listowanie produktów to nie widzimy w tym problemu.

Wiemy, że w niektórych miejscach zastosowanie niektórych technik będzie przerostem formy nad treścią, więc nie musisz na siłę tego robić wszędzie. 
Wystarczy jak dane rozwiazanie zastosujesz tylko w jednym miejscu, a w pozostałych zostawisz komentarz, że tutaj też można używać takiego rozwiazania.
Nie musisz pisać testów do wszystkich klas - wystarczy tyle ile uznasz, że dobrze prezentuje Twój poziom wiedzy o nich.

Jeżeli na czymś utkniesz, napisz komentarz jak byś to widział, ale nie wyszło i idź dalej. Niedziałająca aplikacja nie będzie dyskwalifikowana. 
Każdy popełnia błędy i każdemu zdażają się chwile pustki w głowie. 

**Sam decydujesz** jak dużo fajnych rzeczy zrobisz w tym projekcie (i ile czasu na niego poświęcisz), jednak pamiętaj, że my nie znamy Twoich umiejętności i będziemy chcieli je poznać właśnie na podstawie Twojej realizacji tego zadania.

### Co dalej?
Wrzuć swój kod do repozytorium; jeżeli będzie on dla nas interesujący zaprosimy Cię na spotkanie online, w którym zrobimy code review na żywo. 
Będziesz miał wtedy również okazję dodać coś od siebie i opowiedzieć o tym, czego nie udało Ci się przekazać za pomocą kodu.

**Powodzenia**
