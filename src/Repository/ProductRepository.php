<?php

namespace App\Repository;

use App\Entity\ProductORM;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\Query;

class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductORM::class);
    }

    /**
     * w $this są metody find() findAll() findBy() itp
     * żeby stworzyć swojego SQLa potrzeba EntityManager który ma metody createQuery createQueryBuilder itp
     */
    public function findAllProducts()
    {
        $entityManager = $this->getEntityManager();

        $qb = $entityManager->createQueryBuilder('p')
            ->select('p')
            ->from('App\Entity\ProductORM', 'p')
            ->where('p.price > 10');

        $query = $qb->getQuery();
        $products = $query->execute();

        $mapped = $this->mapToProducts($products);

        return $mapped;
    }

    public function findById($id)
    {
        $product = $this->find($id);
        $mapped = $this->mapToProducts([$product]);

        // return first (and only) element
        return reset($mapped);
    }



    /**
     * helper method to map all ORM products to domain Products
     */
    public function mapToProducts(array $orm_products): array
    {
        $products = [];
        foreach ($orm_products as $orm_product) {
            $products[] = $orm_product->toProduct();
        }

        return $products;
    }


    public function addProduct(ProductORM $product)
    {
        $entityManager = $this->getEntityManager();

        $entityManager->persist($product);
        $entityManager->flush();

    }

    public function delete(ProductORM $product)
    {
        $entityManager = $this->getEntityManager();

        $entityManager->remove($product);
        $entityManager->flush();
    }
}