<?php

namespace App\Controller;

use App\Entity\ProductORM;
use App\Form\Type\ProductFormType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\ProductRepository;


class ProductController extends AbstractController
{

    /**
     * @Route("/show/{id}")
     */
    public function show(?int $id, ProductRepository $productRepository)
    {
        $product = $productRepository->findById($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        return $this->render('product.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/add")
     */
    public function add(Request $request, ProductRepository $productRepository)
    {
        $form = $this->createForm(ProductFormType::class, new ProductORM());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();

            $productRepository->addProduct($product);

            $this->addFlash('success', 'Produkt został dodany.');
            return $this->redirectToRoute('homepage');
        }

        return $this->render('add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/remove/{id}")
     */
    public function remove(?int $id, ProductRepository $productRepository)
    {
        $product = $productRepository->find($id);
        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $productRepository->delete($product);

        $this->addFlash('success', 'Produkt został usunięty.');
        return $this->redirectToRoute('homepage');
    }

}
