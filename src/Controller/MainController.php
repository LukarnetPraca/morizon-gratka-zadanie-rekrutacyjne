<?php

namespace App\Controller;

use App\Entity\Poland;

use App\Repository\ProductRepository;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Doctrine\Persistence\ManagerRegistry;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     *
     * autowire repository
     */
    public function index(ProductRepository $productRepository): Response
    {
        //$all = $productRepository->findAll();
        $all2 = $productRepository->findAllProducts();

        return $this->render('list.html.twig', [
            'products' => $all2,
            'country' => new Poland(),
        ]);
    }


}