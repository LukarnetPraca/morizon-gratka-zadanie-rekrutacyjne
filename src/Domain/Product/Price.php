<?php

namespace App\Domain\Product;

/**
 * Value object used in DDD Product
 * Business does not care about the actual type of price
 */
final class Price
{
    private int $price;

    public function __construct(int $price) {

        if ($price < 0) {
            throw new \Exception('Price cannot be negative');
        }

        $this->price = $price;
    }

    public function value(): int
    {
        return $this->price;
    }
}