<?php

namespace App\Domain\Product;

use App\Domain\Taxable;
use App\Entity\Country;

/**
 * Domain-Driven Design of a Product
 * all primitive data types hidden in Value Objects
 */
class Product implements ProductBase, Taxable
{
    private ProductId $id;
    private Name $name;
    private Price $price;
    private PriceEur $price_eur;
    private Description $description;

    public function __construct(
        ProductId $id,
        Name $name,
        Price $price,
        PriceEur $price_eur,
        Description $description
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->price_eur = $price_eur;
        $this->description = $description;
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function name(): Name
    {
        return $this->name;
    }

    public function price(): Price
    {
        return $this->price;
    }

    public function price_eur(): PriceEur
    {
        return $this->price_eur;
    }

    public function description(): Description
    {
        return $this->description;
    }

    public function calculateTax(Country $country)
    {
        return round($this->price()->value() * ($country->getTaxValue() / 100), 2);
    }
}
