<?php

namespace App\Domain\Product;

/**
 *
 */
final class ProductId
{
    private int $id;

    public function __construct(int $id) {
        $this->id = $id;
    }

    public function value(): int
    {
        return $this->id;
    }
}