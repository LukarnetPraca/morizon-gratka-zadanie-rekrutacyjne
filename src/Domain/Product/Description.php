<?php

namespace App\Domain\Product;

/**
 * Value object used in DDD Product
 * Business does not care about the actual type of description
 */
final class Description
{
    /**
     * can be null if no description given
     */
    private ?string $description;

    public function __construct(?string $description) {
        $this->description = $description;
    }

    public function value(): ?string
    {
        return $this->description;
    }
}