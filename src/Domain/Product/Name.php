<?php

namespace App\Domain\Product;

/**
 * Value object used in DDD Product
 * Business does not care about the actual type of name
 */
final class Name
{
    private string $name;

    public function __construct(string $name) {
        $this->name = $name;
    }

    public function value(): string
    {
        return $this->name;
    }
}