<?php

namespace App\Domain\Product;

interface ProductBase
{
    public function id(): ProductId;
    public function name(): Name;
    public function price(): Price;
    public function price_eur(): PriceEur;
    public function description(): Description;
}
