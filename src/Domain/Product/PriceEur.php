<?php

namespace App\Domain\Product;

final class PriceEur
{
    private int $price;

    public function __construct(int $price) {

        if ($price < 0) {
            throw new \Exception('Price cannot be negative');
        }

        $this->price = $price;
    }

    public function value(): int
    {
        return $this->price;
    }
}