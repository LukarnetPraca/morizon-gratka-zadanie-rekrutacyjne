<?php

namespace App\Domain;

use App\Entity\Country;

/**
 * sample interface that a Product must adhere to
 */
interface Taxable
{
    public function calculateTax(Country $country);
}