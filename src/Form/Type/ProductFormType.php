<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class, ['attr' => ['class' => 'form-control'], 'required' => true]);
        $builder->add('price', TextType::class, ['attr' => ['class' => 'form-control']]);
        $builder->add('price_eur', TextType::class, ['attr' => ['class' => 'form-control']]);
        $builder->add('description', TextareaType::class, ['attr' => ['class' => 'form-control']]);
    }
}