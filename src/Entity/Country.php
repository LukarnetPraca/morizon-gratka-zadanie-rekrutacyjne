<?php

namespace App\Entity;

/**
 * sample abstract class for Taxable interface
 */
abstract class Country
{
    protected $tax;

    public function getTaxValue(): ?int
    {
        return $this->tax;
    }
}