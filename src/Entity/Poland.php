<?php

namespace App\Entity;

define('TAX_VALUE_PL', 7);

class Poland extends Country
{
    public function __construct() {
        $this->tax = TAX_VALUE_PL;
    }
}