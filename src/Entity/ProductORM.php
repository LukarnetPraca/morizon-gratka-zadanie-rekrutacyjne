<?php

namespace App\Entity;

use App\Domain\Product\Product;
use App\Domain\Product\ProductId;
use App\Domain\Product\Name;
use App\Domain\Product\Price;
use App\Domain\Product\PriceEur;
use App\Domain\Product\Description;

use Doctrine\ORM\Mapping as ORM;

/**
 * ORM implementation for DDD Product
 */

/**
 * @ORM\Entity(repositoryClass=App\Repository\ProductRepository::class)
 * @ORM\Table(name="products")
 */
class ProductORM
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    public $name;

    /**
     * @ORM\Column(type="integer")
     */
    public $price;

    /**
     * @ORM\Column(type="integer")
     */
    public $price_eur;

    /**
     * @ORM\Column(type="text", length=10000)
     */
    public $description;


    /**
     * return Domain Product generated from an ORM Product
     * @return Product
     */
    public function toProduct(): Product
    {
        return new Product(
            new ProductId($this->id),
            new Name($this->name),
            new Price($this->price),
            new PriceEur($this->price_eur),
            new Description($this->description)
        );
    }
}