<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210205123332 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'sample products';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("insert into products (name, price) values ('hub asmbl', 19999)");
        $this->addSql("insert into products (name, price) values ('hub seal', 299)");
        $this->addSql("insert into products (name, price) values ('knuckle seal', 899)");
        $this->addSql("insert into products (name, price) values ('inner bearing', 5769)");
        $this->addSql("insert into products (name, price) values ('outer bearing', 3969)");
        $this->addSql("insert into products (name, price) values ('seal-dust', 959)");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('truncate table products;');
    }
}
