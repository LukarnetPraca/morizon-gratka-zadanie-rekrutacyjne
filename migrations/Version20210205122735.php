<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210205122735 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'db inti';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('create table products
(
	id int auto_increment,
	name varchar(150) null,
	price int default 0 null,
	constraint products_pk
		primary key (id)
);');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
