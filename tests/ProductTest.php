<?php

namespace App\Tests\Product;

use PHPUnit\Framework\TestCase;


use App\Domain\Product\Product;
use App\Domain\Product\ProductId;
use App\Domain\Product\Description;
use App\Domain\Product\Name;
use App\Domain\Product\Price;
use App\Domain\Product\PriceEur;
use App\Domain\Taxable;
use App\Entity\Country;

// php bin/phpunit tests/

final class ProductTest extends TestCase
{
    public function testProductId(): void
    {
        $result = new ProductId(3);
        $this->assertEquals(3, $result->value());
    }

    public function testProductName(): void
    {
        $result = new Name('Produkt');
        $this->assertEquals('Produkt', $result->value());
    }

    public function testProductPrice(): void
    {
        $result = new Price(1230);
        $this->assertEquals(1230, $result->value());
    }

    public function testProductNegativePrice(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Price cannot be negative");

        $result = new Price(-5);
    }

    public function testProductDescription(): void
    {
        $result = new Description('To jest opis');
        $this->assertEquals('To jest opis', $result->value());
    }

    public function testProductCreation(): void
    {
        $result = new Product(
            new ProductId(1),
            new Name('Produkt'),
            new Price(1234),
            new PriceEur(987),
            new Description('To jest opis')
        );

        $this->assertEquals(1,          $result->id()->value());
        $this->assertEquals('Produkt',  $result->name()->value());
        $this->assertEquals(1234,  $result->price()->value());
        $this->assertEquals(987,  $result->price_eur()->value());
        $this->assertEquals('To jest opis',  $result->description()->value());
    }


}

